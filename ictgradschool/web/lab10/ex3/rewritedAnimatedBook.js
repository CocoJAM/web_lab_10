/**
 * Created by ljam763 on 6/04/2017.
 */
"use script";

function addEventHandler(element, nameOfEvent, handler) {

    if (element.addEventListener) {
        element.addEventListener(nameOfEvent, handler, false);
        console.log(1);
    } else if (element.bind) {
        element.bind("on" + nameOfEvent, handler);
        console.log(2);
    } else {
        nameOfEvent = "on" + nameOfEvent;
        console.log(3);
        element.ready(function () {
            console.log(4);
            return handler
        });
    }
}

function bringNextPageToFront(currPageNum) {
    console.log(currPageNum);
    var nextPageNum = currPageNum + 1;
    return function () {
        var nextPage = $("#page" + nextPageNum);
        if (nextPage) {
            nextPage.css("zIndex", 1);
        }
    };
}

function doLoad() {
    var pages = $(".page");
    var page;
    var pageNum = -0;
    console.log(pages.length);
    pages.each(function () {

        $(this).css("animation-delay", "0s");
        $(this).bind("click", function () {
            if (this.classList) {
                this.classList.add("animatePage");
            } else {
                this.className += " animatePage";
            }
        })
        $(this).bind("webkitAnimationEnd", bringNextPageToFront(pageNum));
        $(this).bind("animationend", bringNextPageToFront(pageNum));
        pageNum++;
    })
}
$("window").ready(doLoad());

