/**
 * Created by ljam763 on 6/04/2017.
 */
"use script";

function makePageInteractive() {
    var container = $("#container");
    var baubles = $(".bauble");
    var i = -1;


    baubles.each(function(){
        console.log(baubles[this]);
        baubles.mouseover(function(){
            if (this.classList) {
                this.classList.add("fall");
            } else {
                this.className += " fall"; // fallback way. Don't forget the space to separate class
                // note that "class" is a reserved word, so the property 'className' has been defined for the class attribute instead
            }
            }
        )
    })

}

//window.onload = makePageInteractive; // don't call the function here, assign it as the handler.
// That is, don't use the round function call brackets: window.onload = makePageInteractive();

if (window.addEventListener) {
    $("window").ready(makePageInteractive());
} else if (window.attachEvent) {
    $("window").bind('ready', makePageInteractive());
} else {
    $("window").ready( makePageInteractive);
}

